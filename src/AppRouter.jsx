import React, { Component} from 'react'
import {Route, BrowserRouter } from 'react-router-dom';
import Induk from './Induk'
import AppAbout from './components/AppAbout';
// import CardList from './components/CardList';
import './'
export default class AppRouter extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Route exact path="/" component={Induk} />
                    <Route path='/about' component={AppAbout}/>
                    {/* <Route path='/cardlist' component={CardList}/> */}
                </div>
            </BrowserRouter>
        )
    }
}