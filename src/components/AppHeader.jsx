import React, { Component } from 'react';
import{ Link } from 'react-router-dom'
import { Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Container } from 'reactstrap'

    

class AppHeader extends Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
      render() {
        return (
          <div>
            <Navbar color="dark" light expand="md">
              <Container>
              <NavbarBrand className='text-white'><Link to="/">Laptop Store</Link></NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem >
                    <NavLink className='text-white'><Link to='/'>Home</Link></NavLink>
                  </NavItem>
                  <NavItem >
                    <NavLink className='text-white'><Link to='/'>Produk List</Link></NavLink>
                  </NavItem>
                  <NavItem >
                    <NavLink className='text-white'><Link to='/about'>About</Link></NavLink>
                  </NavItem>
                </Nav>
              </Collapse>
              </Container>
            </Navbar>
          </div>
        );
      }
}

export default AppHeader;