import React from 'react';
import AppHeader from './components/AppHeader';
// import AppBody from './components/AppBody';
import AppFooter from './components/AppFooter';
import AppCarousel from './components/common/AppCarousel';
// import './assets/style/Main.css';

 
class Induk extends React.Component{
    render(){
        return(
            <div className="induk">
                <AppHeader />
                {/* <AppBody /> */}
                <AppCarousel />
                <AppFooter />
            </div>
        )
    }
}

export default Induk;